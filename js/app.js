'use strict';
(function () {
    window.app = {};

    function start(el) {
        app.instance = new Vue({
            el: el,
            data: {
                connectionStatus: 'offline', // online or offline
                currentTab: 'tv'
            },
            components: {
                tv: getTV(),
                matrix: getMatrix()
            },
            methods: {
                fixIphoneTapHighlight() {
                    // https://stackoverflow.com/questions/3885018/active-pseudo-class-doesnt-work-in-mobile-safari/33681490#33681490
                }
            },
            created: function () {
                connect(this);
            }
        });
    };

    function getTV() {
        return {
            name: 'tv',
            template: '#tmpl-tv'
        };
    }

    function getMatrix() {
        return {
            name: 'tv',
            template: '#tmpl-matrix',
            data: function () {
                return {
                    currentSource: null,
                    source: getSource(),
                    output: getOutput()
                };  
            },
            methods: {
                toggleSource: function (output) {
                    var context = this;
                    var currentSource = context.currentSource;
                    if(!output.source || output.source.name !== currentSource.name) {
                        output.source = toJSON(currentSource);
                    } else {
                        output.source = null;
                    }
                }
            },
            created: function () {
                var context = this;
                context.currentSource = context.source[0];
            }
        };
    }

    function getSource() {
        return  [
            {
                name: 'tv',
                desc: '卫星电视'
            },
            {
                name: 'meeting',
                desc: '视频会议'
            },
            {
                name: 'app',
                desc: '应用系统'
            }
        ];
    } 

    function getOutput() {
        return [
            {
                name: 'screen-1',
                desc: '1号大屏',
                source: null
            },
            {
                name: 'screen-2',
                desc: '2号大屏',
                source: null
            },
            {
                name: 'screen-3',
                desc: '3号大屏',
                source: null
            },
            {
                name: 'screen-4',
                desc: '4号大屏',
                source: null
            },
            {
                name: 'meeting',
                desc: '视频会议',
                source: null
            }
        ];
    }

    function connect(context) {
        context.connectionStatus = 'online';
    }

    function toJSON(data) {
        if(typeof data === 'undefined') {
            return; 
        }
        if(typeof data === 'object') {
            data = JSON.stringify(data);
        }
        return JSON.parse(data);
    }

    app.start = start;

})();
